// ################
// ЗАГРУЗКА СТРАНИЦЫ
// ################
$(document).ready(function(){
	var data = localStorage.data ? JSON.parse(localStorage.data) : [];
		function getValuesForData(){
		var id = +new Date();
		var title = $('#input_title').val();
		var status = false;
		var resultObj = {
		  "id": id,
		  "title": title,
		  "status": status,
		};
		return resultObj;
	};
// ################
// ЗАГРУЗКА ДИВОВ
// ################
	function rowCheckboxValues(i){
		if(data[i].status){
			$('#checkbox_mainblock'+i).prop("checked",true);
		}else{ 		
			$('#checkbox_mainblock'+i).prop("checked",false);
		};
	};
//загрузка линий mainblock
	function loadDivRow(){
	$('.row').remove();

		for (var i=0, len=data.length; i<len; i++) {

			$("#mainblock").append('<div class="row"><input type="checkbox" class="checkbox_mainblock" id="checkbox_mainblock'+i+'"><div class="title_mainblock" id="title_mainblock'+i+'"></div><div class="delete_buttons" id="delete'+i+'"></div></div>');

			rowCheckboxValues(i);

			$('#title_mainblock'+i).html(data[i].title);	
		};
	};

	loadDivRow();
// обновление значений
	function reloadValuesInMainblock(){

		for (var i=0, len=data.length; i<len; i++) {

			rowCheckboxValues(i);

			$('#title_mainblock'+i).html(data[i].title);
		}
	};
// ################
// НАЖАТИЕ КНОПОК
// ################

//add row
	function addNewRow(){

		i =  data.length-1;

		$("#mainblock").append('<div class="row"><input type="checkbox" class="checkbox_mainblock" id="checkbox_mainblock'+i+'"><div class="title_mainblock" id="title_mainblock'+i+'"></div><div class="delete_buttons" id="delete'+i+'"></div></div>');

		$('#title_mainblock'+i).html(data[i].title);
	};
//кнопка добавить
	$('#input_title').keydown(function(event){
		//если это Enter
		if(event.keyCode == 13) {
			addData();
			$('#input_title').blur();
		}
		//если это Esc
		if(event.keyCode == 27) {
			$('#input_title').val('');
			$('#input_title').blur();
		}
	});
	function addData(){

		console.log(checkRadio());
		$("#radio_all").prop({'checked': true});

		if($('#input_title').val().trim() == ''){
			console.log("пустая строка!");
			return false;
		}

		data.push(getValuesForData());
		localStorage.data = JSON.stringify(data);//пиши её
		console.log(data.length);
		console.log(data[data.length-1]);
		reloadValuesInMainblock();

		addNewRow();
		$('#input_title').val('');
	};
	$('#add').click(function(){
		addData();
		loadDivRow();
	});
//delete row
	$('body').on('click','.delete_buttons',function(){ 

		var idNumberCatch = this.id.substring(6,this.id.length);

		console.log(idNumberCatch);
		data.splice(idNumberCatch,1);
		localStorage.data = JSON.stringify(data);//пиши её
		reloadValuesInMainblock();
		$('.row:last').remove();

	});
//radio visible
	function checkRadio(){
		if($("#radio_all").prop("checked")){
			return "all"
		};
		if($("#radio_compl").prop("checked")){
			return "compl"			
		};
		if($("#radio_uncompl").prop("checked")){
			return "uncompl"
		};
	};
//смена свойства display у линий
	function rowVisible(){
		//all
		if(checkRadio() == "all"){
			$('.row').css('display','inline-block');
		};
		//compl
		if(checkRadio()== "compl"){
			for (var i=0, len=data.length; i<len; i++) {
			 	if(!($('#checkbox_mainblock'+i).prop("checked"))){
				  $('.row').eq(i).css('display','none');
				}else{
				  $('.row').eq(i).css('display','inline-block');
				}
			};
		};
		//uncompl
		if(checkRadio()== "uncompl"){
			for (var i=0, len=data.length; i<len; i++) {
			 	if($('#checkbox_mainblock'+i).prop("checked")){
				  $('.row').eq(i).css('display','none');
				}else{
				  $('.row').eq(i).css('display','inline-block');
				};
			};
		};
	};
//радиокнопки
	$('[name = all_compl_uncompl]').click(function(){
		rowVisible();
	});
//delete all
	$('#delete_all').click(function(){
		if (confirm('удалить все данные?')){

			$('.row').remove();
			data = [];
			localStorage.data = JSON.stringify(data);//пиши её

		}else{
		console.log('хорошо, что спросил');
		};
	});	
//delete comlp
	$('#delete_compl').click(function(){
		var j = 0;//количество удалений
		for (var i=0, len=data.length; i<len; i++) {
			if(data[i-j].status){
				data.splice(i-j,1);
				j++;//количество удалений
			};
		localStorage.data = JSON.stringify(data);
		};
	loadDivRow();
	$("#radio_all").prop({'checked': true});
	});
// ################
// РЕДАКТОР СТАТУСА
// ################
	$('body').on('click','.checkbox_mainblock',function(e){

		var idcatch = this.id;
		var idNumberCatch = this.id.substring(18,this.id.length);
		console.log(idNumberCatch);
		var checkboxValue = $('#'+idcatch).prop('checked');

		if(checkboxValue){
			data[idNumberCatch].status = true;
			localStorage.data = JSON.stringify(data);//пиши её всегда после изменения дата
		}else{
			data[idNumberCatch].status = false;
			localStorage.data = JSON.stringify(data);//пиши её всегда после изменения дата
		};
	});
// ################
// РЕДАКТОР TITLE
// ################
	$('body').on('dblclick','.title_mainblock',function(e){
		var idNumberCatch = this.id.substring(15,this.id.length);
		console.log(this.id.substring(15,this.id.length));
		//если это инпут - ничего не делаем
		var val = $(this).html();
		var code = '<input type="text" id="edit" value="'+val+'" />';
		$(this).empty().append(code);
		$('#edit').focus();

		$('#edit').keydown(function(event){
			//если это Enter
			if(event.keyCode == 13) {	
				$('#edit').blur();
			};
			//если это Esc
			if(event.keyCode == 27) {	
				$('#edit').val(data[idNumberCatch].title);
				$('#edit').blur();
			};
		});
		
		$('#edit').blur(function(){
		
			var val = $(this).val();

			if(val.trim() != ''){

				data[idNumberCatch].title = val;
				localStorage.data = JSON.stringify(data);//пиши меня, ну ты знаешь...
			}else{

				console.log('пустая строка! верну старое значение');
				$('#title'+idNumberCatch).html(data[idNumberCatch].title);
				reloadValuesInMainblock();
			}

			$(this).parent().empty().html(val);
		});
	});

});